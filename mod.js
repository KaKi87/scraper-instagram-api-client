import axios from 'axios';

export const setUrl = url => axios.defaults.baseURL = url;

export const getPartialProfile = async ({ username }) => (await axios(`/partialProfile/${username}`)).data;

export const getProfile = async ({ username }) => (await axios(`/profile/${username}`)).data;

export const getPartialPost = async ({ shortcode }) => (await axios(`/partialPost/${shortcode}`)).data;

export const getPost = async ({ shortcode }) => (await axios(`/post/${shortcode}`)).data;